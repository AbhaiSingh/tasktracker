package sample.model;

import java.util.ArrayList;
import java.util.List;

public class Tasks {
    private List<Task> taskList;

    public Tasks() {
    }

    public Tasks(List<Task> taskList) {
        this.taskList = taskList;
    }

    public List<Task> getTaskList() {
        if(taskList==null){
            this.taskList = new ArrayList<>();
        }
        return taskList;
    }

    public void addToTaskList(Task task) {
        this.taskList.add(task);
    }
}
