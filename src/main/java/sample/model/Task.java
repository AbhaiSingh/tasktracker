package sample.model;

import java.util.UUID;

public class Task {
    private String id;
    private String details;
    private long createdDate;
    private boolean isCompleted;

    public Task(){}
    public Task(String details, long createdDate) {
        this.details = details;
        this.createdDate = createdDate;
        id = UUID.randomUUID().toString();
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return details;
    }
}
