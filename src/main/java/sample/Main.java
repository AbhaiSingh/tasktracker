package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * https://docs.oracle.com/javafx/2/get_started/css.htm#BABBGJBI
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/main_view.fxml"));
        Scene scene = new Scene(root,800,500);
        primaryStage.setTitle("Task tracker");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
            launch(args);
    }
}
