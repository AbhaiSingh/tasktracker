package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import org.controlsfx.control.CheckListView;
import sample.dataAccessLayer.FileResourceProvider;
import sample.dataAccessLayer.ResourceProvider;
import sample.model.Task;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Currently data is persisted in text files.
 * On initialization verify if text file exists and load current day's task in memory from it.
 * All new tasks to be updated in the memory and written in file.
 * If on initialization file doesn't exists create the file.
 * <p>
 * {
 * "tasks": [
 * {
 * "details": "Hey first task",
 * "date": 21102018
 * },
 * {
 * "details": "Hey second task",
 * "date": 21102018
 * }
 * ]
 * }
 */
public class Controller implements Initializable {
    @FXML
    private TextField taskToAdd;

    @FXML
    private CheckListView<Task> taskList;

    @FXML
    private DatePicker taskDate;

    private ResourceProvider fileResourceProvider = new FileResourceProvider();
    private ObservableList<Task> taskObservableList = FXCollections.observableArrayList();

    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) {
        String task = taskToAdd.getText();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            long taskCreationDate = dateFormat.parse(taskDate.getValue().toString()).getTime();
            if (task == null || task.trim().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, "Validation", "Please enter some information about the task");
            } else {
                Task taskToAdd = new Task(task, taskCreationDate);
                addTask(taskToAdd);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        refreshElements();
    }

    private void addTask(Task taskToAdd){
        fileResourceProvider.addTask(taskToAdd);
        if(FileResourceProvider.isToday(new Date(taskToAdd.getCreatedDate()))){
            taskObservableList.add(taskToAdd);
            taskList.setItems(taskObservableList);
            taskObservableList.stream()
                    .filter(Task::isCompleted)
                    .forEach(task -> taskList.getCheckModel().check(task));
        }
    }

    private void taskCheckBoxListener(){
        taskList.getCheckModel().getCheckedItems().addListener(new ListChangeListener<Task>() {
            @Override
            public void onChanged(Change<? extends Task> c) {
                for (Task task : taskList.getCheckModel().getCheckedItems()) {
                    task.setCompleted(true);
                    fileResourceProvider.updateTaskCompletion(task.getId());
                }
            }
        });
    }

    private void initializeWithExistingTasks() {
        if(taskObservableList.size() == 0){
            taskObservableList.addAll(fileResourceProvider.getCurrentDayAndAllOpenTasks().getTaskList());
        }
    }

    private void refreshElements() {
        taskToAdd.setText(null);
        taskDate.setValue(LocalDate.now());
    }

    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializeWithExistingTasks();
        taskList.setItems(taskObservableList);
        taskDate.setValue(LocalDate.now());
        taskObservableList.stream()
                .filter(Task::isCompleted)
                .forEach(task -> taskList.getCheckModel().check(task));
        taskCheckBoxListener();
    }
}
