package sample.controller;

import com.jfoenix.controls.JFXTabPane;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;

import java.io.IOException;
import java.net.URL;

public class MainViewController {

    @FXML
    private JFXTabPane tabContainer;

    @FXML
    private Tab currentTaskTab;

    @FXML
    private GridPane currentTaskContainer;

    @FXML
    private Tab archiveTab;

    @FXML
    private GridPane archiveContainer;

    @FXML
    private StackPane tabStackPane;

    private double tabWidth = 70.0;
    private static int lastSelectedTab = 0;

    @FXML
    public void initialize(){
        configureView();
    }

    private void configureView() {
        tabStackPane.setStyle("-fx-background-color: black");
        tabContainer.setTabMinWidth(244);
        tabContainer.setTabMaxWidth(244);
        tabContainer.setTabMinHeight(70);
        tabContainer.setTabMaxHeight(70);
        tabContainer.setStyle("-fx-background-color: black");
        tabContainer.setRotateGraphic(true);

        EventHandler<Event> replaceBackgroundColorHandler = event->{
            lastSelectedTab =tabContainer.getSelectionModel().getSelectedIndex();
            Tab currentTab = (Tab) event.getTarget();
            if (currentTab.isSelected()) {
                currentTab.setStyle("-fx-background-color: -fx-focus-color;");
            } else {
                currentTab.setStyle("-fx-background-color: -fx-accent;");
            }
        };

        configureTab(currentTaskTab,"Current\nTask",currentTaskContainer,getClass().getResource("/currentTask.fxml"),replaceBackgroundColorHandler);
        configureTab(archiveTab,"Archive",archiveContainer,getClass().getResource("/archive.fxml"),replaceBackgroundColorHandler);

    }

    private void configureTab(Tab tab, String title, GridPane containerPane, URL resourceURL, EventHandler<Event> onSelectionChangedEvent) {
        Label label = new Label(title);
        label.setMaxWidth(tabWidth - 20);
        label.setPadding(new Insets(5, 0, 0, 0));
        label.setStyle("-fx-text-fill: chartreuse; -fx-font-size: 10pt; -fx-font-weight: bold;");
        label.setTextAlignment(TextAlignment.CENTER);

        BorderPane tabPane = new BorderPane();
        tabPane.setRotate(90.0);
        tabPane.setMaxWidth(tabWidth);
        tabPane.setBottom(label);

        tab.setText("");
        tab.setGraphic(tabPane);

        tab.setOnSelectionChanged(onSelectionChangedEvent);

        if (containerPane != null && resourceURL != null) {
            try {
                Parent contentView = FXMLLoader.load(resourceURL);
                containerPane.getChildren().add(contentView);
                AnchorPane.setTopAnchor(contentView, 0.0);
                AnchorPane.setBottomAnchor(contentView, 0.0);
                AnchorPane.setRightAnchor(contentView, 0.0);
                AnchorPane.setLeftAnchor(contentView, 0.0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
