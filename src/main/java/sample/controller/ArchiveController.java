package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import sample.dataAccessLayer.FileResourceProvider;
import sample.dataAccessLayer.ResourceProvider;
import sample.model.Task;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ArchiveController implements Initializable {

    @FXML
    private DatePicker fromDate;

    @FXML
    private DatePicker toDate;

    @FXML
    private ListView taskList;

    private ResourceProvider fileResourceProvider = new FileResourceProvider();
    private ObservableList<Task> taskObservableList = FXCollections.observableArrayList();

    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (fromDate == null || toDate == null) {
                showAlert(Alert.AlertType.ERROR, "Validation", "Please enter dates");
            }
            long fromTime = dateFormat.parse(fromDate.getValue().toString()).getTime();
            long toTime = dateFormat.parse(toDate.getValue().toString()).getTime();
            taskObservableList.addAll(fileResourceProvider.getTasksInDateRange(fromTime, toTime).getTaskList());
            taskList.setItems(taskObservableList);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void showAlert(Alert.AlertType alertType, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setContentText(message);
        alert.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fromDate.setValue(LocalDate.now());
        toDate.setValue(LocalDate.now());
    }
}
