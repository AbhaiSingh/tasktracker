package sample.dataAccessLayer;

import org.codehaus.jackson.map.ObjectMapper;
import sample.Main;
import sample.model.Task;
import sample.model.Tasks;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FileResourceProvider implements ResourceProvider {
    private static final String filePath = new File("").getAbsolutePath()+"/CurrentTasks.json";
    private static Tasks taskList;
    private static ObjectMapper mapper = new ObjectMapper();
    private static File taskFile;

    static {
        taskFile = new File(filePath);
        if (taskFile.exists()) {
            try {
                taskList = mapper.readValue(taskFile, Tasks.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                taskFile = new File( filePath);
                System.out.println("path "+taskFile.getAbsolutePath());
                taskFile.createNewFile();
                taskList = new Tasks();
                mapper.writeValue(taskFile,taskList);
            } catch (IOException e) {
                throw new RuntimeException("Failed in creating file " + e.toString());
            }
        }
    }

    @Override
    public Tasks getCurrentDayTasks() {
        List<Task> filteredTasks = taskList.getTaskList()
                .stream()
                .filter(task -> isToday(new Date(task.getCreatedDate())))
                .collect(Collectors.toList());
        return new Tasks(filteredTasks);
    }

    @Override
    public Tasks getCurrentDayAndAllOpenTasks() {
        List<Task> filteredTasks = taskList.getTaskList()
                .stream()
                .filter(task -> !task.isCompleted())
                .collect(Collectors.toList());
        Tasks todayTask = getCurrentDayTasks();
        todayTask.getTaskList().addAll(filteredTasks);
        return todayTask;
    }

    @Override
    public Tasks getTasksInDateRange(long fromDate, long toDate){
        List<Task> filteredTasks = taskList.getTaskList()
                .stream()
                .filter(task -> isInRange(task.getCreatedDate(),fromDate,toDate))
                .collect(Collectors.toList());
        return new Tasks(filteredTasks);
    }

    @Override
    public void addTask(Task taskToadd) {
        taskList.addToTaskList(taskToadd);
        try {
            writeToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateTaskCompletion(String taskId) {
        taskList.getTaskList().stream()
                .filter(task -> task.getId().equals(taskId))
                .forEach(task -> task.setCompleted(true));
        try {
            writeToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>Checks if a date is today.</p>
     *
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    private static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    private static boolean isInRange(long taskDate, long fromDate, long toDate){
        Calendar taskCalender = Calendar.getInstance();
        taskCalender.setTime(new Date(taskDate));
        Calendar fromCalender = Calendar.getInstance();
        fromCalender.setTime(new Date(fromDate));
        Calendar toCalender = Calendar.getInstance();
        toCalender.setTime(new Date(toDate));

        if(taskCalender.get(Calendar.YEAR) >= fromCalender.get(Calendar.YEAR) &&
                taskCalender.get(Calendar.YEAR) <= toCalender.get(Calendar.YEAR)){
            if(taskCalender.get(Calendar.DAY_OF_YEAR) >= fromCalender.get(Calendar.DAY_OF_YEAR) &&
                    taskCalender.get(Calendar.DAY_OF_YEAR) <= toCalender.get(Calendar.DAY_OF_YEAR)){
                return true;
            }
        }
        return false;
    }

    /**
     * <p>Checks if two calendars represent the same day ignoring time.</p>
     *
     * @param cal1 the first calendar, not altered, not null
     * @param cal2 the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    private static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    private void writeToFile() throws IOException {
        mapper.writeValue(taskFile, taskList);
    }
}
