package sample.dataAccessLayer;

import sample.model.Task;
import sample.model.Tasks;

public interface ResourceProvider {
    Tasks getCurrentDayTasks();
    Tasks getCurrentDayAndAllOpenTasks();
    Tasks getTasksInDateRange(long fromDate, long toDate);
    void addTask(Task taskToadd);
    void updateTaskCompletion(String taskId);
}
