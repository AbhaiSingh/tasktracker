import sample.dataAccessLayer.FileResourceProvider;
import sample.dataAccessLayer.ResourceProvider;
import sample.model.Task;

import java.util.Calendar;

public class TestJsonParsing {

    public static void main(String a[]){
        ResourceProvider fileResource = new FileResourceProvider();
        fileResource.getCurrentDayTasks().getTaskList().forEach(task -> System.out.println(task.getDetails()));
        Task taskTom = new Task();
        taskTom.setDetails("Test task tomorrow");
        Calendar cal = Calendar.getInstance();
        //cal.add(Calendar.DAY_OF_MONTH,5);
        taskTom.setCreatedDate(cal.getTime().getTime());
        fileResource.addTask(taskTom);
        System.out.println("*************************************************************");
        fileResource.getCurrentDayTasks().getTaskList().forEach(task -> System.out.println(task.getDetails()));
    }

}
