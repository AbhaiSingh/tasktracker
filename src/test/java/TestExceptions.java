import java.io.IOException;
import java.net.ConnectException;

public class TestExceptions {

    public static void main(String a[]){
        try{
            System.out.println("Available processors: "+Runtime.getRuntime().availableProcessors());
            callExceptionPropagation();
        } catch (RuntimeException e){
            if(e.getCause() instanceof ConnectException){
                System.out.println("Connect Exception propagated");
            } if(e.getCause() instanceof IOException){
                System.out.println("IO Exception propagated");
            }
        }
    }

    private static void callExceptionPropagation(){
        try {
            callException();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void callException() throws ConnectException {
        throw new ConnectException("dude u don't have connection");
    }
}
